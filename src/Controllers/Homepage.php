<?php
namespace DemoSite\Controllers;

use DemoSite\Model\Services\User as UserService;
use Http\Request;
use Http\Response;
use DemoSite\View\Homepage as View;

class Homepage
{
    private $request;
    private $response;

    public function __construct(
        Request $request,
        Response $response,
        View $view,
		UserService $userService
    ) {
        $this->request 		= $request;
        $this->response 	= $response;
        $this->view 		= $view;
        $this->userService 	= $userService;
    }

	/**
	 * @return bool
	 */
    public function createUser() : bool {
    	$userData = $this->request->getParameters();
    	return $this->userService->addUser($userData);
	}

    public function show()
    {
        $this->response->setContent($this->view->getHtml());
    }
}