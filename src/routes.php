<?php
return [
    ['GET', '/', ['DemoSite\Controllers\Homepage', 'show']],
    ['POST', '/', ['DemoSite\Controllers\Homepage', 'createUser']]
];