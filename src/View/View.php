<?php
/**
 * Created by PhpStorm.
 * User: Fabien
 * Date: 29/11/2017
 * Time: 04:07
 */

namespace DemoSite\View;


interface View
{
	public function getHtml();
}