<?php

namespace DemoSite\View;

interface Renderer
{
    public function render($template, $data = []);
}