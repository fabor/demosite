<?php

namespace DemoSite\View;

use DemoSite\Menu\MenuReader;

class FrontendTwigRenderer implements FrontendRenderer
{
    private $renderer;
    private $menuReader;

    public function __construct(Renderer $renderer)
    {
        $this->renderer = $renderer;
    }

    public function render($template, $data = [])
    {
        $data = array_merge($data, [
            'menuItems' => ['home', 'about'],
        ]);
        return $this->renderer->render($template, $data);
    }
}