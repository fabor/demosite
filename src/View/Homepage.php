<?php

namespace DemoSite\View;

use DemoSite\Model\Services\User as UserService;

class Homepage implements View
{
	private $renderer;
	private $userService;

	public function __construct(
		FrontendRenderer $renderer,
		UserService $userService
	)
	{
		$this->renderer		= $renderer;
		$this->userService 	= $userService;
	}

	/**
	 * @return mixed
	 */
	public function getHtml()
	{
		$data = [
			'name' => $this->userService->getUserById('1')->getName(),
		];

		return $this->renderer->render('index', $data);
	}
}