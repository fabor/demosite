<?php
$injector = new \Auryn\Injector;
$injector->alias('Http\Response', 'Http\HttpResponse');
$injector->share('Http\HttpRequest');
$injector->define('Http\HttpRequest', [
    ':get' => $_GET,
    ':post' => $_POST,
    ':cookies' => $_COOKIE,
    ':files' => $_FILES,
    ':server' => $_SERVER,
]);
$injector->alias('Http\Request', 'Http\HttpRequest');
$injector->share('Http\HttpResponse');

$injector->delegate('Twig_Environment', function() use ($injector) {
    $loader = new Twig_Loader_Filesystem(dirname(__DIR__) . '/public/templates');
    $twig = new Twig_Environment($loader, array(
        'debug' => true
    ));
    $twig->addExtension(new Twig_Extension_Debug());
    return $twig;
});
$injector->alias('DemoSite\View\Renderer', 'DemoSite\View\TwigRenderer');
$injector->alias('DemoSite\View\FrontendRenderer', 'DemoSite\View\FrontendTwigRenderer');

$connectionParams = [
	'dbname' => 'demosite',
	'user' => 'root',
	'password' => '123',
	'host' => 'localhost',
	'driver' => 'pdo_mysql',
];
$connection = \Doctrine\DBAL\DriverManager::getConnection($connectionParams, $injector->make('Doctrine\DBAL\Configuration'));
$injector->share($connection);

return $injector;