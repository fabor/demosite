<?php

namespace DemoSite\Model\Entities;

final class User
{
	private $id;
	private $name;
	private $email;
	private $password;

	public function __clone() {
		unset($this->id);
	}

	/**
	 * @return mixed
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @return mixed
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * @param mixed $name
	 */
	public function setName($name)
	{
		$this->name = $name;
	}

	/**
	 * @return mixed
	 */
	public function getEmail()
	{
		return $this->email;
	}

	/**
	 * @param mixed $email
	 */
	public function setEmail($email)
	{
		$this->email = $email;
	}

	/**
	 * @return mixed
	 */
	public function getPassword()
	{
		return $this->password;
	}

	/**
	 * @param mixed $password
	 */
	public function setPassword($password)
	{
		$this->password = $password;
	}

	/**
	 * @param array $data
	 * @return User
	 */
	public function hydrate(array $data = []) : User {
		$this->setName($data['name']);
		$this->setEmail($data['email']);
		$this->setPassword($data['password']);

		return $this;
	}
}