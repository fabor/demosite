<?php

namespace DemoSite\Model\Mappers;

use DemoSite\Model\Entities\User as UserEntity;

final class User
{
	public function __construct(\Doctrine\DBAL\Connection $connection) {
		$this->connection = $connection;
	}

	/**
	 * @param int $id
	 * @return mixed
	 */
	public function getUserById(int $id) {
		$stmt = $this->connection->prepare("SELECT * FROM users WHERE id = :id");
		$stmt->execute([':id' => $id]);

		return $stmt->fetch(\PDO::FETCH_ASSOC);
	}

	/**
	 * @param string $email
	 * @return mixed
	 */
	public function getUserByEmail(string $email = '') {
		$stmt = $this->connection->prepare("SELECT * FROM users WHERE email = :email");
		$stmt->execute([':email' => $email]);

		return $stmt->fetch(\PDO::FETCH_ASSOC);
	}

	public function persist(UserEntity $userEntity) : bool {
		$stmt = $this->connection->prepare("INSER INTO users(name, email, password) VALUES(:name, :email, :password");
		$stmt->bindValue(':name', $userEntity->getName());
		$stmt->bindValue(':email', $userEntity->getEmail());
		$stmt->bindValue(':password', $userEntity->getPassword());

		$stmt->execute();

		return $stmt->rowCount();;
	}
}