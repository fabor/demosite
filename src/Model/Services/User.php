<?php

namespace DemoSite\Model\Services;

use \DemoSite\Model\Mappers\User as UserMapper;
use \DemoSite\Model\Entities\User as UserEntity;

final class User
{
	public function __construct(UserMapper $userMapper, UserEntity $userEntity)
	{
		$this->userMapper = $userMapper;
		$this->userEntity = $userEntity;
	}

	public function getUserById(int $id) : UserEntity {
		$userData = $this->userMapper->getUserById($id);
		return $this->userEntity->hydrate($userData);
	}

	public function addUser(array $userData = []) : UserEntity {
		$user = (new UserEntity())->hydrate($userData);
		$user->userMapper->persist($user);
	}
}